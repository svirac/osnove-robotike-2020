function TA0est = rvboxpose(P,f,TC0,zC,c)

uc = 320;
vc = 240;

point(1) = 1;
for i=1:3
    dist(i) = sqrt(((P(1,i+1)-P(1,1))^2+((P(2,i+1)-P(2,1))^2)));       
end

% tocke 1 i 4 cine dugacku stranicu (a)
% tocke 1 i 2 cine kratku stranicu (b)
% tocke 1 i 3 su nasuprotne
point(4) = find(dist==median(dist)) + 1;
point(3) = find(dist==max(dist))+1;
point(2) = find(dist==min(dist))+1;

p14(1) = P(1,point(4))-P(1,1);
p14(2) = P(2,point(4))-P(2,1);

% orijentacija
alfa = atan2(p14(2),p14(1));
%kut = 180/pi * alfa;

RAC = [cos(alfa) -sin(alfa) 0;
       sin(alfa) cos(alfa) 0;
       0 0 1];

% koordiante centra mase objekta
ucm = (P(1,1) + P(1,2) + P(1,3) + P(1,4))/4;
vcm = (P(2,1) + P(2,2) + P(2,3) + P(2,4))/4;

% poza
tAC = [(ucm-uc)*(zC-c)/f;
       (vcm-vc)*(zC-c)/f;
       zC-c/2];

% polo�aj
TAC = [RAC tAC;
       0 0 0 1];

TA0est = TC0*TAC;