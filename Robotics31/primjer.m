%OSNOVE ROBOTIKE LV1: DIREKTNA KINEMATIKA


close all;
clear;
clc;



  
%**********Create robot****************************

%broj osi robota
robot.n=6;

%tip zgloba 
%(0->translacijski 1->rotacijski)
ksi=[1 1 1 1 1 1];

%kinematicki parametri  q (theta)-parametri zglobova
% original  q=[pi/2 -pi/2 pi/2 0 0 -pi/2];  
% redom ides po onoj tablici i upisujes parametre po stupcu za svaki
q=[pi/2 -pi/2 pi/2 0 0 -pi/2];  

d=[0 0 0 300 0 150];
a=[0 240 0 0 0 0];
alpha=[-pi/2 0 pi/2 -pi/2 pi/2 0];

% kad se transponira, dobije se ona tablica parametara
DH=[q; d; a; alpha]';

% za svaki zglob definiramo tip zgloba i njegova 4 parametra
for i=1:robot.n
    robot.L(i).ksi=ksi(i);
    robot.L(i).DH=DH(i,:);
end

        %centar mase | dimenzije kvadra%
robot.L(1).B=[0 120 0 60 300 60];
          
robot.L(2).B=[-120 0 60 300 60 60];

robot.L(3).B=[0 0 120 60 60 300];

robot.L(4).B=[0 10 0 20 40 20];

robot.L(5).B=[0 20 10 20 20 40
              0 -20 10 20 20 40
              0 0 50 60 60 40];

robot.L(6).B=[0 0 -60 20 20 40
              0 0 -35 30 100 10
              0 35 -15 20 10 30
              0 -35 -15 20 10 30];


robot.g=[0 0 9.81]';



robmesh = robotmesh(robot, q);

figure;
plot3dobj(robmesh)

%Plot the base of the manipulator
%The base is defined with respect to L0
%center of the manipulator base is (0 0 -280)
% ovo je ona baza, nije rotirana, na -280 od L0
Tbase=[1 0 0 0
       0 1 0 0
       0 0 1 -280 
       0 0 0 1];
   
base=cuboid(180,180,20);
base.X= Tbase*base.X;
hold on;
plot3dobj(base);


%create T wrt L0.
% izracuna onu matricu T60
T = eye(4); 
for i = 1:robot.n
    pDH = robot.L(i).DH;
    % dhtransf racuna onu matricu Tii-1
    T = T * dhtransf(pDH);
end



% kvadar A: dimenzije a x b x c
% matrica transformacije iz koordinatnog sustava 6 u koordinatni sustav A
%(rotacija oko osi z6 za 90 stupnjeva + translacija centar mase po z)
TA6=[0 -1 0 0;
      1 0 0 0;
      0 0 1 -5;
      0 0 0 1];

% polozaj objekta u odnosu na bazni KS
TA0=T*TA6;


%predmet
obj=cuboid(40,20,50);
obj.X=TA0*obj.X;

hold on;
plot3dobj(obj);
