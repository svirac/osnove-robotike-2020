%OSNOVE ROBOTIKE LV3: PLANIRANJE TRAJEKTORIJE


close all;
clear;
clc;


createRobot;

% Wr -> referentne tocke

Wr = [0 -65 2260; %start
    1300 -100 500; %primak
    
    1400 -100 500; %ravna linija
    1400 -100 400;
    1400 -100 300;
    1400 -100 200;
    1400 -100 100;
    1400 -100 20;
    
    1400 -50 0; %kruzic
    1400 20 50;
    1400 -50 100;
   
    1400 -200 0; %stomak
    1400 -300 20;
    1400 -400 100;  
    1400 -450 300; 
    1400 -400 500;
    1400 -300 560;
    1400 -200 600;
    1400 -100 600;
    1400 -40 550;
    1400 -10 400;
    1400 -10 300;
    
    1300 -10 300; %odmak
    0 -65 2260;]'; %stop 


% broj definiranih tocaka
m = length(Wr);

% T60 za prvu tocku
T60(:,:,1) = [1 0 0 Wr(1,1);
			  0 1 0 Wr(2,1);
			  0 0 1 Wr(3,1);
			  0 0 0 1];

% rotacija po y za 90�
% definiramo matrice T60 za ostale tocke
for i = 2:m-1
    T60(:,:,i) = [0 0 1 Wr(1,i);
				  0 1 0 Wr(2,i);
				  -1 0 0 Wr(3,i);
				  0 0 0 1];
end

% T60 za zadnju tocku
T60(:,:,m) = [1 0 0 Wr(1,m);
              0 1 0 Wr(2,m);
              0 0 1 Wr(3,m);
              0 0 0 1];

% varijable zglobova za svaku definiranu tocku
Q = [];

for i = 1:m

    if i==1 || i == m   % za prvu i zadnju tocku
        Q = [Q invkin(T60(:,:,i))];
    else
        Q = [Q invkin(T60(:,:,i),Q(:,end))];
    end
    
	% provjera trenutni i prethodni stupac da je uvijek izme�u 0 i pi ili 0 i -pi 
    if i > 1
        for j = 1:6
            if Q(j,i) - Q(j,i-1) > pi
                Q(j,i) = Q(j,i) - 2 * pi;
            else
                if Q(j,i) - Q(j,i-1) < -pi
                   Q(j,i) = Q(j,i) + 2 * pi;
                end
            end
        end
    end
end

Q = [Q; zeros(1, m)];

% max vrijednosti brzine
dqgr = [6 6 6 6 6 6 100]';
% 7. element predstavlja ogranicenje na brzinu zatvaranja hvataljke (ne igra neku veliku ulogu)
ddqgr = [15 15 15 15 15 15 10000]';
% Zasto bas te vrijednosti? -> Pokazale se kao optimalne.

% f-ja hocook odre�uje jos me�utocaka izme�u nasih definiranih tocaka; *upute za toolbox str 7.
% dqgr prva derivacije; ddqgr druga derivacija; -> predstavljaju max vrijednost ubrzanja za varijablu zgloba
% Ts vrijeme uzorkovanja trajektorije = 0.002
% Qc -> sve nove tocke i me�utocke; puno veci broj stupaca, svaki stupac je jedna tocka; u prostoru varijabli zglobova
% dQc i ddQc -> vrijednosti brzine i ubrzanja kroz te tocke
[Qc, dQc, ddQc] = hocook(Q, dqgr, ddqgr, 0.002);

% broj novih tocaka
nt = length(Qc);

W = [];

% dirkin -> direktna kinematika; dobijemo polozaj vrha alata u prostoru alata za sve tocke trajektorije
% robot.n -> zadnji zglob; T(1:3,4) -> prva 3 reda 4. stupca
% spremamo polozaj vrha alata u matricu W
for k = 1:nt
    robot = dirkin(robot,Qc(1:6,k));
    W = [W robot.L(robot.n).T(1:3,4)];
end

% u zadnjem redu su vremena uzorkovanja zato Qc(8,:); a Qc(1:6,:) crta varijable zglobova
figure(1)
plot(Qc(8,:)',Qc(1:6,:)')
% brzine zglobova
figure(2)
plot(Qc(8,:)',dQc(1:6,:)')
% ubrzanja zglobova
figure(3)
plot(Qc(8,:)',ddQc(1:6,:)')

% animacija robota kroz tocke Qc
% [-800 1600 -1000 1400 -800 2300] -> volumen prostora koji zelimo promatrati
figure(4)
dyn3dscene(robot, Qc, [], [-800 1600 -1000 1400 -800 2300], 145, 30, 0.05,0.001)

hold on
% 3D; zelenom bojom ofarbane izracunate tocke, a crvenom bojom referentne tocke
plot3(W(1,:),W(2,:),W(3,:),'g',Wr(1,:),Wr(2,:),Wr(3,:),'ro'),axis equal

hold off

figure(5)

% normala ravnine u x smjeru okomita na xz ravninu
N = [1 0 0]';
% udaljenost od ravnine
% 450 -> ne smije bit preblizu ni predaleko; vjj cu morat malo povecat
d = 1400;

% vidimo sve tocke unutar tolerancije 5 od te ravnine
% koliko je precizna putanja alata bila
planecontact(W,N,d)