% Broj osi
robot.n = 6;

% Zglobovi ('0'-translacijski, '1'-rotacijski)
ksi = [1 1 1 1 1 1];

% Kinematicki parametri
q = [pi/2 -pi/2 pi/2 0 0 -pi/2];
d = [0 0 0 1025 0 365];
a = [145 870 -210 0 0 0];
alfa = [-pi/2 0 pi/2 -pi/2 pi/2 0];

for i=1:robot.n
    robot.L(i).ksi=ksi(i);
end

% Clanci u odnosu na K.S. i njihove dimenzije
robot.L(1).B = [-145 382.5 0 455 75 455
                -145 125 152.5 455 440 150
                -145 125 -152.5 455 440 150];

robot.L(2).B = [-457.5 0 0 1115 290 155];

robot.L(3).B = [210  127.5 75 290 100 350
                210 -127.5 75 290 100 350
                84 0 300 542 355 100];

robot.L(4).B = [0 312.5 0 114 725 114];

robot.L(5).B = [0 82 30 114 50 160
                0 -82 30 114 50 160
                0 0 142.5 114 214 65];

robot.L(6).B = [0 0 -160 30 30 60
                0 0 -115 30 290 30
                0 130 -50 30 30 100
                0 -130 -50 30 30 100];

% Gravitacija
robot.g = [0 0 9.810];

% Tablica s tim parametrima
DH=[q; d; a; alfa]';

% Pridruzi svakom zglobu njegove parametre
for i=1:robot.n
    robot.L(i).DH=DH(i,:);
end

robmesh = robotmesh(robot, q);