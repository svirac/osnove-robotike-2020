function [Qc, TA0est, Q] = rvtraj(P,f,TP0,TCP,c)

% box pose estimation

TC0 = TP0*TCP;

TA0est = rvboxpose(P,f,TC0,TCP(3,4),c);

% initial robot configuration

% nulti polozaj predmeta
T60(:,:,1) = [1 0 0 0;
    0 1 0 -65;
    0 0 1 2260; 
    0 0 0 1];

% approach configuration 

% vrh alata u odnosu na predmet
T6A = [1 0 0 0;
    0 1 0 0;
    0 0 1 -2*c;
    0 0 0 1];

T60(:,:,2) = TA0est*T6A;

% goal configuration

% vrh alata poravnat s predmetom
T6A = [1 0 0 0;
       0 1 0 0;
       0 0 1 0;
       0 0 0 1];

T60(:,:,3) = TA0est*T6A;

% trajectory

m = 3;

Q = [];

for i = 1:m
    Q = [Q invkin(T60(:,:,i))];
    
    if i > 1
        for j = 1:6
            if Q(j,i) - Q(j,i-1) > pi
                Q(j,i) = Q(j,i) - 2 * pi;
            else
                if Q(j,i) - Q(j,i-1) < -pi
                   Q(j,i) = Q(j,i) + 2 * pi;
                end
            end
        end
    end
end

    
Q = [Q; 50 50 50];


dqgr = [6 6 6 6 6 6 100]';
ddqgr = [15 15 15 15 15 15 1000]';

[Qc, dQc, ddQc] = hocook(Q, dqgr, ddqgr, 0.002);

nt = length(Qc);