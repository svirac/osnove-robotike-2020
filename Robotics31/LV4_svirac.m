% The following parameters must be defined before running function rvsim:
%
%     method     - image processing method: 'corners' or 'edges'. Default is
%                  'edges'.
%     xmin, xmax - working range, i.e. minimum and maximum x-coordinate 
%                  which can be reached by the tool 
%     zT0        - height of the working plane relative to the base
%                  c. s. of the robot

clear;
close all;
clc;

% na kojoj visini postavljamo plohu u odnosu na radni prostor
zT0 = 500; 

% min i max vrijednosti gdje ce se nalaziti taj predmet
% xmin = 50;  
% xmax=400; 
xmin = 1300;  
xmax= 1800;

method ='corners';

rvsim;