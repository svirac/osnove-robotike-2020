clear;
close all;
clc;

% environment map

map.dim = 2;

map.X = [
    0, 0;       % 1
    160, 0;     % 2
    160, 160;   % 3     
    0, 160;     % 4
    5, 5;       % 5
    155, 5;     % 6
    155, 155;   % 7
    5, 155;     % 8
    45, 155;    % 9
    50, 155;    % 10
    45, 45;     % 11
    50, 45;     % 12
    50, 50;     % 13
    115, 50;    % 14
    115, 45;    % 15
    110, 50;    % 16
    110, 85;    % 17
    115, 85;    % 18
    50, 115;    % 19
    50, 110;    % 20
    75, 115;    % 21
    75, 110;    % 22
    110, 115;   % 23
    110, 110;   % 24
    155, 110;   % 25
    155, 115;   % 26    
    ]'*0.05;

map.S = [
    % okvir
    1, 2, 6;
    1, 6, 5;
    2, 3, 7;
    2, 7, 6;
    3, 4, 8;
    3, 8, 7;
    4, 1, 5;
    4, 5, 8;
    
    9, 11, 12;
    9, 12, 10;
    
    12, 15, 14;
    12, 14, 13;
    
    14, 18, 17;
    14, 17, 16;
    
    20, 22, 21;
    20, 21, 19;
    
    24, 25, 26;
    24, 26, 23;   
    ]';

map = edges2(map);

map.nL = 0;

% robot

robot = createmobrob();

x0 = [25*0.05 135*0.05 -pi/2]';     % A

robot.mem = [1; x0];  % mobrobctrlalg -> [iPt we]

% controller

ctrlparam.T = 0.05;
ctrlparam.vmax = 0.5;
ctrlparam.rho0 = 1;
ctrlparam.eta = 1e-20;
ctrlparam.path = [
    40, 25;     % B
    120, 25;    % C
    120, 97.5;  % D
    105, 97.5;  % E
    105, 135;    % F
    135, 135;   % H
    ]'*0.05;


sim('mobrob_sim.mdl');

hold on; plot(x(:,1),x(:,2),'g'); hold off 
