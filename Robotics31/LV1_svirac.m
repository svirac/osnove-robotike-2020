%OSNOVE ROBOTIKE LV1: DIREKTNA KINEMATIKA

close all;
clear all;
clc

createRobot;

figure;
plot3dobj(robmesh)

% Baza
Tbase = [1 0 0 0
         0 1 0 -76.5
         0 0 1 -480
         0 0 0 1];
     
base = cuboid(608,608,120);  
base.X = Tbase*base.X;
 
hold on;
plot3dobj(base);
 
T = eye(4);
for i = 1:robot.n
    pDH = robot.L(i).DH;
    T = T * dhtransf(pDH);
end
 
TA6=[1 0 0 0;
      0 1 0 0;
      0 0 1 -25;
      0 0 0 1];

TA0=T*TA6;

skaliranje = 3;
A=40*skaliranje; B=20*skaliranje; C=50*skaliranje;
obj=cuboid(A,B,C);
obj.X=TA0*obj.X;

hold on;
plot3dobj(obj);