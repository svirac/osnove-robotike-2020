function q_best = min_offset(Q, T60)

min_error = 100000;
createRobot;
for j=1:size(Q,2);
    
    q = Q(:,j);
    DH=[q d' a' alfa'];  
    for i=1:robot.n
       robot.L(i).DH=DH(i,:);
    end
    robmesh = robotmesh(robot, q);
    T = eye(4); 
    for i = 1:robot.n
        pDH = robot.L(i).DH;
        T = T * dhtransf(pDH);
    end
    differnce = T60 - T;
    
    error = sum(sumabs(differnce));
    if error < min_error
        min_error = error;
        index = j;
    end
end
q_best = Q(:,index);
end

 