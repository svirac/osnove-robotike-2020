function q = invkin(T60, qPrev)

% Parametri
p = T60 * [0 0 -365 1]';
x = p(1);
y = p(2);
z = p(3);
a1 = 145;
a2 = 870;
a3 = -210;
d4 = 1025;

% Formule
syms u 'real';
c3 = (1-u^2)/(1+u^2);
s3 = (2*u)/(1+u^2);
f1 = d4*s3 + a3*c3 + a2;
f2 = a3*s3 - d4*c3;
k1 = f1^2 + f2^2 + a1^2;

Q = [];

% theta3
eqn3 = (x^2 + y^2 + z^2 - k1)^2/(4*a1^2) + z^2 == f1^2 + f2^2;
u3 = real(double(solve(eqn3)));
for i=1:size(u3,2)
    if isempty(u3)
        q3 = pi/2;
    else
        q3 = 2*atan(u3(i)); 
    end 
    if mod(i,2) == 0
        if q3 >= 0
            q3 = pi - q3;
        else
            q3 = -pi - q3;
        end
    end
    s3 = sin(q3);
    c3 = cos(q3);
    f1 = d4*s3 + a3*c3 + a2;
    f2 = a3*s3 - d4*c3;
    
    % theta2
    c2 = (1-u^2)/(1+u^2);
    s2 = (2*u)/(1+u^2);
    eqn2 = z == -f1*s2 - f2*c2;
    u2 = double(solve(eqn2));
    for j = 1:2
        if isempty(u2)
            q2 = 0;
        else
            q2 = 2*atan(u2(j));
        end
        s2 = sin(q2);
        c2 = cos(q2);
        g1 = c2*f1 - s2*f2 + a1;
        
        % theta1
        q1 = atan2(y/g1, x/g1);

        % THETA4-6
        % rotacijske matrice  
        T10 = dhtransf([q1 0 145 -pi/2]);
        T21 = dhtransf([q2 0 870 0]);
        T32 = dhtransf([q3 0 -210 pi/2]);
        T30 = T10*T21*T32;
        R30 = T30(1:3,1:3);
        R60 = T60(1:3,1:3);

        r =R30'*R60;
        
        for k = 1:2
            if k == 1
                q5 = acos(r(3,3));
            else
                q5 = -acos(r(3,3));
            end
            
            s5 = sin(q5);

            if s5 == 0
                q4 = 0;
                q6 = acos(r(1,1));
            else
                q4 = atan2(r(2,3)/s5, r(1,3)/s5);
                q6 = atan2(r(3,2)/s5, -r(3,1)/s5);
            end

            q = [q1 q2 q3 q4 q5 q6]';           
            
            Q = [Q q];
        end       
    end
end

if nargin == 1
    %q_best = Q(:,3);
    % find q with minimum offset
    q = min_offset(Q, T60);
else
%     dQ = Q-qPrev*ones(1,size(Q,2));
%     
%     for i=1:size(Q,1)
%         for j=1:size(Q,2)
%             if dQ(i,j) > pi
%                 dQ(i,j) = dQ(i,j) - 2*pi;
%             else
%                 if dQ(i,j) < -pi
%                     dQ(i,j) = dQ(i,j) + 2*pi;
%                 end
%             end
%         end
%     end
%     
%     [tmp, i] = min(max(abs(dQ)));    
%     
%     q = qPrev + dQ(:,i);
    q = Q(:,3);
    %q = min_offset(Q, T60);
end

end