%OSNOVE ROBOTIKE LV2: INVERZNA KINEMATIKA

close all;
clear;
clc;

% kvadar A: dimenzije a x b x c
% X0A polozaj sredista kvadra u odnosu na bazni koordinatni sustav
% alpha je kut zakreta kvadra u odnosu na z-os
skaliranje = 3;
A=40*skaliranje; B=20*skaliranje; C=50*skaliranje;
%XA0=[600 1500 -465]' - lijepo izgleda
XA0=[0 700 900]'; % -465 da bude na tlu;
alphaA=0;

%matrica transformacije iz koordinatnog sustava A u bazni koordin. sustav
ca = cos(alphaA);
sa = sin(alphaA);
%rotacija zbog kuta alphaA (kut kvadra u odnosu na z-os)
RA0 = [ca -sa 0
       sa ca 0
       0 0 1];   

%jos je koordinatni sustav A translatiran za X0A u odnosu na bazni
TA0 = [RA0 XA0; 0 0 0 1];         

%matrica transformacije iz koordinatnog sustava 6 u koordinatni sustav A
%(rotacija oko osi x6 za 180 stupnjeva + translacija po z)
T6A=[1 0 0 0;
      0 -1 0 0;
      0 0 -1 -25;
      0 0 0 1];
%=>
T60=TA0*T6A;

createRobot;

q = invkin(T60);

for i=1:robot.n
   robot.L(i).DH=DH(i,:);
end

robmesh = robotmesh(robot, q);

T = eye(4); 
for i = 1:robot.n
    pDH = robot.L(i).DH;
    T = T * dhtransf(pDH);
end
provjera1 = T60 - T;

% matrica transformacije iz koordinatnog sustava A u koordinatni sustav 6
% TA6=[T6A(1:3,1:3)' T6A(1:3,4);0 0 0 1];
% (rotacija oko osi xA za 180 stupnjeva + translacija po z)
TA6=[1 0 0 0;
      0 -1 0 0;
      0 0 -1 -25;
      0 0 0 1];
  
TA0new=T*TA6;
provjera2 = TA0 - TA0new;



%plot the robot
plot3dobj(robmesh);

%baza
Tbase = [1 0 0 0
         0 1 0 -76.5
         0 0 1 -480
         0 0 0 1];
     
base = cuboid(608,608,120);  
base.X = Tbase*base.X;

hold on;
plot3dobj(base);

%predmet
obj=cuboid(A,B,C);
obj.X=TA0*obj.X;
env.object(1)=obj;

%add the enviroment (scene)
hold on;
plot3dobj(obj);